'use strict';




var app = angular.module('form-example1', []);

var INTEGER_REGEXP = /^\-?\d+$/;
app.directive('integer', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$validators.integer = function(modelValue, viewValue) {
                if (ctrl.$isEmpty(modelValue)) {
                    // consider empty models to be valid
                    return true;
                }

                if (INTEGER_REGEXP.test(viewValue)) {
                    var dig = (""+viewValue).split("");
                    var kontrola = (1*parseInt(dig[0]) + 3*parseInt(dig[1]) + 7*parseInt(dig[2]) + 9*parseInt(dig[3]) + 1*parseInt(dig[4]) + 3*parseInt(dig[5]) + 7*parseInt(dig[6]) + 9*parseInt(dig[7]) + 1*parseInt(dig[8]) + 3*parseInt(dig[9]))%10;
                    if(kontrola==0) kontrola = 10;
                    kontrola = 10 - kontrola;
                    if(parseInt(dig[10])==kontrola)
                        return true;
                }
                // invalid
                var reg = /^[0-9]{11}$/;
                if (reg.test(viewValue) == false) {
                    return false;
                }
            };
        }
    };
});
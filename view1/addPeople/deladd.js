'use strict';

var dlt = angular.module('dlt', []);

dlt.controller("delController", ['$http', '$scope', '$q', function delController($http, $scope, $q) {


    $scope.delIt = function () {
        var promises = [];

        angular.forEach($scope.people, function (item) {
            if (item.Selected) {
                promises.push($http.delete("http://localhost:3000/users/" + item.id));
            }
        });


        $q.all(promises).then(function (response) {
            $scope.updateTable();
        }, function (reason) {
            alert('rizonuje');
        }, function (update) {
            alert('updejtuje');
        });

    }
}]);




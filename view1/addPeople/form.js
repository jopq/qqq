'use strict';

angular.module('formUser', [])

    .directive('myDialog', function ($http) {
            return {
                restrict: 'E',
                replace: true,
                scope: {},
                templateUrl: 'view1/addPeople/addtempl.html',
                link: function (scope, $scope) {


                    scope.person = {
                        name: '',
                        surname: '',
                        pesel: '',
                        gender: ''
                    };


                    scope.add = function () {
                        $http.post("http://localhost:3000/users", scope.person).then(function () {
                            scope.$emit('updating');
                            debugger;
                        })
                    }

                }
            }
        }
    );



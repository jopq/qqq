'use strict';

var usersManagement = angular.module('usersManagement', ['ngRoute','check', 'dlt','formUser', 'genderFilter', 'form-example1'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'MainController'
  });
}])


    .controller('MainController', ['$scope', '$http', function($scope, $http) {

    $scope.$on('updating', function(){
        $scope.updateTable()
    } );
        $scope.people = [];
    $scope.updateTable = function(){

        var promise = $http.get("http://localhost:3000/users");
        promise.then(function (response) {
            $scope.people = response.data;
        });

    }

}]);
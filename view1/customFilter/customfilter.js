'use strict';


angular.module('genderFilter', [])
    .filter('genderToImg', function () {


            return function (gender) {
                if (gender === "male") {
                    return "img/male-50.png"
                } else if(gender === "female"){
                    return "img/female-50.png"
                } else{
                    return "img/nonex.png"
                }

            }
        }
    );


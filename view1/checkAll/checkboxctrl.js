'use strict';

var check = angular.module('check',[]);
    check.controller("checkboxController", function checkboxController($scope) {

        $scope.updateTable();


        $scope.checkAll = function () {
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }
                angular.forEach($scope.people, function (item) {
                    item.Selected = $scope.selectedAll;
                });
            };
        });

